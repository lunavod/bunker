<?php /* Smarty version Smarty-3.1.8, created on 2015-09-26 12:16:00
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/blog_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14391365365606625069b0d1-79993160%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '975f350503f2b8753dea1c07fd061d27cd842b37' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/blog_list.tpl',
      1 => 1442680920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14391365365606625069b0d1-79993160',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bBlogsUseOrder' => 0,
    'sBlogsRootPage' => 0,
    'sBlogOrder' => 0,
    'sBlogOrderWayNext' => 0,
    'sBlogOrderWay' => 0,
    'aLang' => 0,
    'oUserCurrent' => 0,
    'aBlogs' => 0,
    'oBlog' => 0,
    'sBlogsEmptyList' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5606625073cf56_72953110',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5606625073cf56_72953110')) {function content_5606625073cf56_72953110($_smarty_tpl) {?><table class="table table-blogs">
	<?php if ($_smarty_tpl->tpl_vars['bBlogsUseOrder']->value){?>
		<thead>
			<tr>
				<th class="cell-name"><a href="<?php echo $_smarty_tpl->tpl_vars['sBlogsRootPage']->value;?>
?order=blog_title&order_way=<?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_title'){?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWayNext']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_title'){?>class="<?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_title'];?>
</a></th>

				<?php if ($_smarty_tpl->tpl_vars['oUserCurrent']->value){?>
					<th class="cell-join"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_join_leave'];?>
</th>
				<?php }?>

				<th class="cell-readers">
					<a href="<?php echo $_smarty_tpl->tpl_vars['sBlogsRootPage']->value;?>
?order=blog_count_user&order_way=<?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_count_user'){?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWayNext']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_count_user'){?>class="<?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_readers'];?>
</a>
				</th>
				<th class="cell-rating align-center"><a href="<?php echo $_smarty_tpl->tpl_vars['sBlogsRootPage']->value;?>
?order=blog_rating&order_way=<?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_rating'){?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWayNext']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['sBlogOrder']->value=='blog_rating'){?>class="<?php echo $_smarty_tpl->tpl_vars['sBlogOrderWay']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_rating'];?>
</a></th>
			</tr>
		</thead>
	<?php }else{ ?>
		<thead>
			<tr>
				<th class="cell-name"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_title'];?>
</th>

				<?php if ($_smarty_tpl->tpl_vars['oUserCurrent']->value){?>
					<th class="cell-join"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_join_leave'];?>
</th>
				<?php }?>

				<th class="cell-readers"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_readers'];?>
</th>
				<th class="cell-rating align-center"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['blogs_rating'];?>
</th>
			</tr>
		</thead>
	<?php }?>
	
	
	<tbody>
		<?php if ($_smarty_tpl->tpl_vars['aBlogs']->value){?>
			<?php  $_smarty_tpl->tpl_vars['oBlog'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oBlog']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aBlogs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oBlog']->key => $_smarty_tpl->tpl_vars['oBlog']->value){
$_smarty_tpl->tpl_vars['oBlog']->_loop = true;
?>
				<?php $_smarty_tpl->tpl_vars["oUserOwner"] = new Smarty_variable($_smarty_tpl->tpl_vars['oBlog']->value->getOwner(), null, 0);?>

				<tr>
					<td class="cell-name">
						<a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
">
							<img src="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getAvatarPath(48);?>
" width="48" height="48" alt="avatar" class="avatar" />
						</a>
						
						<p>
							<a href="#" onclick="return ls.infobox.showInfoBlog(this,<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
);" class="icon-question-sign"></a>

							<?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getType()=='close'){?>
								<i title="<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_closed'];?>
" class="icon-lock"></i>
							<?php }?>

							<a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlog']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>
						</p>
					</td>

					<?php if ($_smarty_tpl->tpl_vars['oUserCurrent']->value){?>
						<td class="cell-join">
							<?php if ($_smarty_tpl->tpl_vars['oUserCurrent']->value->getId()!=$_smarty_tpl->tpl_vars['oBlog']->value->getOwnerId()&&$_smarty_tpl->tpl_vars['oBlog']->value->getType()=='open'){?>
								<a href="#" onclick="ls.blog.toggleJoin(this, <?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
); return false;" class="link-dotted">
									<?php if ($_smarty_tpl->tpl_vars['oBlog']->value->getUserIsJoin()){?>
										<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_leave'];?>

									<?php }else{ ?>
										<?php echo $_smarty_tpl->tpl_vars['aLang']->value['blog_join'];?>

									<?php }?>
								</a>
							<?php }else{ ?>
								&mdash;
							<?php }?>
						</td>
					<?php }?>

					<td class="cell-readers" id="blog_user_count_<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getId();?>
"><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getCountUser();?>
</td>
					<td class="cell-rating align-center"><?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getRating();?>
</td>
				</tr>
			<?php } ?>
		<?php }else{ ?>
			<tr>
				<td colspan="3">
					<?php if ($_smarty_tpl->tpl_vars['sBlogsEmptyList']->value){?>
						<?php echo $_smarty_tpl->tpl_vars['sBlogsEmptyList']->value;?>

					<?php }else{ ?>

					<?php }?>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table><?php }} ?>