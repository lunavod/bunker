<?php /* Smarty version Smarty-3.1.8, created on 2015-09-26 19:34:05
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/actions/ActionTalk/add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3549519035606c8fd466db9-83686495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b95652b90bde1f0d73a5abd417e4cbf5c59896aa' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/actions/ActionTalk/add.tpl',
      1 => 1442680920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3549519035606c8fd466db9-83686495',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LIVESTREET_SECURITY_KEY' => 0,
    'aLang' => 0,
    '_aRequest' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5606c8fd4b8874_46674341',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5606c8fd4b8874_46674341')) {function content_5606c8fd4b8874_46674341($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/function.hook.php';
?><?php $_smarty_tpl->tpl_vars["sidebarPosition"] = new Smarty_variable('left', null, 0);?>
<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->getSubTemplate ('actions/ActionProfile/profile_top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ('menu.talk.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<?php echo $_smarty_tpl->getSubTemplate ('actions/ActionTalk/friends.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo smarty_function_hook(array('run'=>'talk_add_begin'),$_smarty_tpl);?>


<div class="topic" style="display: none;">
	<div class="content" id="text_preview"></div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ('editor.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('sImgToLoad'=>'talk_text','sSettingsTinymce'=>'ls.settings.getTinymceComment()','sSettingsMarkitup'=>'ls.settings.getMarkitupComment()'), 0);?>


<form action="" method="POST" enctype="multipart/form-data">
	<?php echo smarty_function_hook(array('run'=>'form_add_talk_begin'),$_smarty_tpl);?>

	
	<input type="hidden" name="security_ls_key" value="<?php echo $_smarty_tpl->tpl_vars['LIVESTREET_SECURITY_KEY']->value;?>
" />

	<p><label for="talk_users"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['talk_create_users'];?>
:</label>
	<input type="text" class="input-text input-width-full autocomplete-users-sep" id="talk_users" name="talk_users" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['talk_users'];?>
" /></p>

	<p><label for="talk_title"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['talk_create_title'];?>
:</label>
	<input type="text" class="input-text input-width-full" id="talk_title" name="talk_title" value="<?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['talk_title'];?>
" /></p>

	<p><label for="talk_text"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['talk_create_text'];?>
:</label>
	<textarea name="talk_text" id="talk_text" rows="12" class="input-text input-width-full mce-editor markitup-editor input-width-full"><?php echo $_smarty_tpl->tpl_vars['_aRequest']->value['talk_text'];?>
</textarea></p>
	
	<?php echo smarty_function_hook(array('run'=>'form_add_talk_end'),$_smarty_tpl);?>

	
	<button type="submit" class="button button-primary" name="submit_talk_add"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['talk_create_submit'];?>
</button>
	<button type="submit" class="button" name="submit_preview" onclick="jQuery('#text_preview').parent().show(); ls.tools.textPreview('talk_text',false); return false;"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_create_submit_preview'];?>
</button>		
</form>

<?php echo smarty_function_hook(array('run'=>'talk_add_end'),$_smarty_tpl);?>


<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>