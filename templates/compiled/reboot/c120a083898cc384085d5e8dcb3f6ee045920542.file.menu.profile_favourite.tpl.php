<?php /* Smarty version Smarty-3.1.8, created on 2015-09-26 19:34:02
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/menu.profile_favourite.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11706176655606c8fab94519-67436745%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c120a083898cc384085d5e8dcb3f6ee045920542' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/menu.profile_favourite.tpl',
      1 => 1442680920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11706176655606c8fab94519-67436745',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'sMenuSubItemSelect' => 0,
    'oUserProfile' => 0,
    'iCountTopicFavourite' => 0,
    'iCountCommentFavourite' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5606c8fabc7cd9_21190003',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5606c8fabc7cd9_21190003')) {function content_5606c8fabc7cd9_21190003($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/function.hook.php';
?><h3 class="profile-page-header"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_profile_favourites'];?>
</h3>

<ul class="nav nav-pills nav-pills-profile">
	<li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='topics'){?>class="active"<?php }?>>
		<a href="<?php echo $_smarty_tpl->tpl_vars['oUserProfile']->value->getUserWebPath();?>
favourites/topics/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_profile_favourites_topics'];?>
  <?php if ($_smarty_tpl->tpl_vars['iCountTopicFavourite']->value){?> (<?php echo $_smarty_tpl->tpl_vars['iCountTopicFavourite']->value;?>
) <?php }?></a>
	</li>
	<li <?php if ($_smarty_tpl->tpl_vars['sMenuSubItemSelect']->value=='comments'){?>class="active"<?php }?>>
		<a href="<?php echo $_smarty_tpl->tpl_vars['oUserProfile']->value->getUserWebPath();?>
favourites/comments/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_menu_profile_favourites_comments'];?>
  <?php if ($_smarty_tpl->tpl_vars['iCountCommentFavourite']->value){?> (<?php echo $_smarty_tpl->tpl_vars['iCountCommentFavourite']->value;?>
) <?php }?></a>
	</li>

	<?php echo smarty_function_hook(array('run'=>'menu_profile_favourite_item','oUserProfile'=>$_smarty_tpl->tpl_vars['oUserProfile']->value),$_smarty_tpl);?>

</ul>

<?php echo smarty_function_hook(array('run'=>'menu_profile_favourite','oUserProfile'=>$_smarty_tpl->tpl_vars['oUserProfile']->value),$_smarty_tpl);?>

<?php }} ?>