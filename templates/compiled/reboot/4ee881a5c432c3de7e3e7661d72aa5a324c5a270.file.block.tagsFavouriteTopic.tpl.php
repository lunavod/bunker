<?php /* Smarty version Smarty-3.1.8, created on 2015-09-26 19:34:02
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/blocks/block.tagsFavouriteTopic.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12619109815606c8fabde634-79766164%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ee881a5c432c3de7e3e7661d72aa5a324c5a270' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/blocks/block.tagsFavouriteTopic.tpl',
      1 => 1442680920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12619109815606c8fabde634-79766164',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'aFavouriteTopicTags' => 0,
    'oTag' => 0,
    'sFavouriteTag' => 0,
    'oFavouriteUser' => 0,
    'aFavouriteTopicUserTags' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5606c8fac3f693_25078761',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5606c8fac3f693_25078761')) {function content_5606c8fac3f693_25078761($_smarty_tpl) {?><?php if (!is_callable('smarty_function_hook')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/function.hook.php';
?><section class="block block-type-foldable block-type-favourite-topic">
	<header class="block-header">
		<h3><a href="#" class="link-dotted" onclick="jQuery('#block_favourite_topic_content').toggle(); return false;"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_favourite_tags_block'];?>
</a></h3>
	</header>
	
	
	<div class="block-content" id="block_favourite_topic_content">
		<ul class="nav nav-pills">
			<li class="active js-block-favourite-topic-tags-item" data-type="all"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_favourite_tags_block_all'];?>
</a></li>
			<li class="js-block-favourite-topic-tags-item" data-type="user"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['topic_favourite_tags_block_user'];?>
</a></li>

			<?php echo smarty_function_hook(array('run'=>'block_favourite_topic_tags_nav_item'),$_smarty_tpl);?>

		</ul>
		
		
		<div class="js-block-favourite-topic-tags-content" data-type="all">
			<?php if ($_smarty_tpl->tpl_vars['aFavouriteTopicTags']->value){?>
				<ul class="tag-cloud word-wrap">
					<?php  $_smarty_tpl->tpl_vars['oTag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oTag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aFavouriteTopicTags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oTag']->key => $_smarty_tpl->tpl_vars['oTag']->value){
$_smarty_tpl->tpl_vars['oTag']->_loop = true;
?>
						<li><a class="tag-size-<?php echo $_smarty_tpl->tpl_vars['oTag']->value->getSize();?>
 <?php if ($_smarty_tpl->tpl_vars['sFavouriteTag']->value==$_smarty_tpl->tpl_vars['oTag']->value->getText()){?>tag-current<?php }?>" title="<?php echo $_smarty_tpl->tpl_vars['oTag']->value->getCount();?>
" href="<?php echo $_smarty_tpl->tpl_vars['oFavouriteUser']->value->getUserWebPath();?>
favourites/topics/tag/<?php echo rawurlencode($_smarty_tpl->tpl_vars['oTag']->value->getText());?>
/"><?php echo $_smarty_tpl->tpl_vars['oTag']->value->getText();?>
</a></li>
					<?php } ?>
				</ul>
			<?php }else{ ?>
				<div class="notice-empty"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_tags_empty'];?>
</div>
			<?php }?>
		</div>
		
		<div class="js-block-favourite-topic-tags-content" data-type="user" style="display: none;">
			<?php if ($_smarty_tpl->tpl_vars['aFavouriteTopicUserTags']->value){?>
				<ul class="tag-cloud word-wrap">
					<?php  $_smarty_tpl->tpl_vars['oTag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oTag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aFavouriteTopicUserTags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oTag']->key => $_smarty_tpl->tpl_vars['oTag']->value){
$_smarty_tpl->tpl_vars['oTag']->_loop = true;
?>
						<li><a class="tag-size-<?php echo $_smarty_tpl->tpl_vars['oTag']->value->getSize();?>
" title="<?php echo $_smarty_tpl->tpl_vars['oTag']->value->getCount();?>
" href="<?php echo $_smarty_tpl->tpl_vars['oFavouriteUser']->value->getUserWebPath();?>
favourites/topics/tag/<?php echo rawurlencode($_smarty_tpl->tpl_vars['oTag']->value->getText());?>
/"><?php echo $_smarty_tpl->tpl_vars['oTag']->value->getText();?>
</a></li>
					<?php } ?>
				</ul>
			<?php }else{ ?>
				<div class="notice-empty"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_tags_empty'];?>
</div>
			<?php }?>
		</div>
	</div>
</section><?php }} ?>