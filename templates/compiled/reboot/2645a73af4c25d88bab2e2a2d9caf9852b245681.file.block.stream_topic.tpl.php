<?php /* Smarty version Smarty-3.1.8, created on 2015-09-28 14:32:52
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/blocks/block.stream_topic.tpl" */ ?>
<?php /*%%SmartyHeaderCode:38427919856092564495054-67279412%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2645a73af4c25d88bab2e2a2d9caf9852b245681' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/blocks/block.stream_topic.tpl',
      1 => 1442833679,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '38427919856092564495054-67279412',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'oTopics' => 0,
    'oTopic' => 0,
    'oUser' => 0,
    'oBlog' => 0,
    'aLang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_56092564588c73_23086300',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56092564588c73_23086300')) {function content_56092564588c73_23086300($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/w/windro/reboot/public_html/engine/lib/external/Smarty/libs/plugins/modifier.truncate.php';
if (!is_callable('smarty_function_date_format')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/function.date_format.php';
if (!is_callable('smarty_modifier_declension')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/modifier.declension.php';
if (!is_callable('smarty_function_router')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/function.router.php';
?><ul class="item-list">
	<?php  $_smarty_tpl->tpl_vars['oTopic'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oTopic']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['oTopics']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oTopic']->key => $_smarty_tpl->tpl_vars['oTopic']->value){
$_smarty_tpl->tpl_vars['oTopic']->_loop = true;
?>
		<?php $_smarty_tpl->tpl_vars["oUser"] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getUser(), null, 0);?>							
		<?php $_smarty_tpl->tpl_vars["oBlog"] = new Smarty_variable($_smarty_tpl->tpl_vars['oTopic']->value->getBlog(), null, 0);?>
		
		<li class="js-title-topic" title="<?php echo htmlspecialchars(smarty_modifier_truncate(trim(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['oTopic']->value->getText())),150,'...'), ENT_QUOTES, 'UTF-8', true);?>
">
			<a href="<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getUserWebPath();?>
"></a>
			
			<a href="<?php echo $_smarty_tpl->tpl_vars['oUser']->value->getUserWebPath();?>
" class="author"><?php echo $_smarty_tpl->tpl_vars['oUser']->value->getLogin();?>
</a> &rarr;
			<a href="<?php echo $_smarty_tpl->tpl_vars['oBlog']->value->getUrlFull();?>
" class="blog-name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oBlog']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a> &rarr;
			<a href="<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getUrl();?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oTopic']->value->getTitle(), ENT_QUOTES, 'UTF-8', true);?>
</a>
			
			<p>
				<time datetime="<?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oTopic']->value->getDate(),'format'=>'c'),$_smarty_tpl);?>
"><?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oTopic']->value->getDateAdd(),'hours_back'=>"12",'minutes_back'=>"60",'now'=>"60",'day'=>"day H:i",'format'=>"j F Y, H:i"),$_smarty_tpl);?>
</time> |
				<?php echo $_smarty_tpl->tpl_vars['oTopic']->value->getCountComment();?>
 <?php echo smarty_modifier_declension($_smarty_tpl->tpl_vars['oTopic']->value->getCountComment(),$_smarty_tpl->tpl_vars['aLang']->value['comment_declension'],'russian');?>

			</p>
		</li>						
	<?php } ?>				
</ul>


<footer>
	<a href="<?php echo smarty_function_router(array('page'=>'index'),$_smarty_tpl);?>
new/"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_stream_topics_all'];?>
</a> | <a href="<?php echo smarty_function_router(array('page'=>'rss'),$_smarty_tpl);?>
new/">RSS</a>
</footer>
					<?php }} ?>