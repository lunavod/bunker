<?php /* Smarty version Smarty-3.1.8, created on 2015-09-26 19:34:05
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/actions/ActionTalk/friends.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15656675735606c8fd4ed453-18004773%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '27a6b79e717c66a2d7d84ee691cef73751c34f8f' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/actions/ActionTalk/friends.tpl',
      1 => 1442680920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15656675735606c8fd4ed453-18004773',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'aUsersFriend' => 0,
    'oFriend' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5606c8fd5412d3_01556126',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5606c8fd5412d3_01556126')) {function content_5606c8fd5412d3_01556126($_smarty_tpl) {?><section class="block block-type-foldable block-type-talk-friends">
	<header class="block-header">
		<h3><a href="#" class="link-dotted" onclick="jQuery('#block_talk_friends_content').toggle(); return false;"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_friends'];?>
</a></h3>
	</header>

	
	<div class="block-content" id="block_talk_friends_content">
		<?php if ($_smarty_tpl->tpl_vars['aUsersFriend']->value){?>
			<ul class="list" id="friends">
				<?php  $_smarty_tpl->tpl_vars['oFriend'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oFriend']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aUsersFriend']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oFriend']->key => $_smarty_tpl->tpl_vars['oFriend']->value){
$_smarty_tpl->tpl_vars['oFriend']->_loop = true;
?>
					<li>
						<input id="talk_friend_<?php echo $_smarty_tpl->tpl_vars['oFriend']->value->getId();?>
" type="checkbox" name="friend[<?php echo $_smarty_tpl->tpl_vars['oFriend']->value->getId();?>
]" class="input-checkbox" /> 
						<label for="talk_friend_<?php echo $_smarty_tpl->tpl_vars['oFriend']->value->getId();?>
" id="talk_friend_<?php echo $_smarty_tpl->tpl_vars['oFriend']->value->getId();?>
_label"><?php echo $_smarty_tpl->tpl_vars['oFriend']->value->getLogin();?>
</label>
					</li>
				<?php } ?>
			</ul>
			
			<footer>
				<a href="#" id="friend_check_all"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_friends_check'];?>
</a> | 
				<a href="#" id="friend_uncheck_all"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_friends_uncheck'];?>
</a>
			</footer>
		<?php }else{ ?>
			<div class="notice-empty"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_friends_empty'];?>
</div>
		<?php }?>
	</div>
</section><?php }} ?>