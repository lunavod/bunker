<?php /* Smarty version Smarty-3.1.8, created on 2015-09-26 12:16:03
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/user_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:37864501656066253221a89-76288950%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a94766bfc4069b183abd6317ee4511244cbca9d5' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/user_list.tpl',
      1 => 1442680920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '37864501656066253221a89-76288950',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bUsersUseOrder' => 0,
    'sUsersRootPage' => 0,
    'sUsersOrder' => 0,
    'sUsersOrderWayNext' => 0,
    'sUsersOrderWay' => 0,
    'aLang' => 0,
    'aUsersList' => 0,
    'oUserList' => 0,
    'oUserNote' => 0,
    'oSession' => 0,
    'sUserListEmpty' => 0,
    'aPaging' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_560662532cca80_19587453',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_560662532cca80_19587453')) {function content_560662532cca80_19587453($_smarty_tpl) {?><?php if (!is_callable('smarty_function_date_format')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/function.date_format.php';
?><table class="table table-users">
	<?php if ($_smarty_tpl->tpl_vars['bUsersUseOrder']->value){?>
		<thead>
			<tr>
				<th class="cell-name"><a href="<?php echo $_smarty_tpl->tpl_vars['sUsersRootPage']->value;?>
?order=user_login&order_way=<?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_login'){?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWayNext']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_login'){?>class="<?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user'];?>
</a></th>
				<th><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_date_last'];?>
</th>
				<th><a href="<?php echo $_smarty_tpl->tpl_vars['sUsersRootPage']->value;?>
?order=user_date_register&order_way=<?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_date_register'){?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWayNext']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_date_register'){?>class="<?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_date_registration'];?>
</a></th>
				<th class="cell-skill"><a href="<?php echo $_smarty_tpl->tpl_vars['sUsersRootPage']->value;?>
?order=user_skill&order_way=<?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_skill'){?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWayNext']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_skill'){?>class="<?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_skill'];?>
</a></th>
				<th class="cell-rating"><a href="<?php echo $_smarty_tpl->tpl_vars['sUsersRootPage']->value;?>
?order=user_rating&order_way=<?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_rating'){?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWayNext']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['sUsersOrder']->value=='user_rating'){?>class="<?php echo $_smarty_tpl->tpl_vars['sUsersOrderWay']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_rating'];?>
</a></th>
			</tr>
		</thead>
	<?php }else{ ?>
		<thead>
			<tr>
				<th class="cell-name"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user'];?>
</th>
				<th class="cell-date"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_date_last'];?>
</th>
				<th class="cell-date"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_date_registration'];?>
</th>
				<th class="cell-skill"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_skill'];?>
</th>
				<th class="cell-rating"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_rating'];?>
</th>
			</tr>
		</thead>
	<?php }?>

	<tbody>
		<?php if ($_smarty_tpl->tpl_vars['aUsersList']->value){?>
			<?php  $_smarty_tpl->tpl_vars['oUserList'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oUserList']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aUsersList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oUserList']->key => $_smarty_tpl->tpl_vars['oUserList']->value){
$_smarty_tpl->tpl_vars['oUserList']->_loop = true;
?>
				<?php $_smarty_tpl->tpl_vars["oSession"] = new Smarty_variable($_smarty_tpl->tpl_vars['oUserList']->value->getSession(), null, 0);?>
				<?php $_smarty_tpl->tpl_vars["oUserNote"] = new Smarty_variable($_smarty_tpl->tpl_vars['oUserList']->value->getUserNote(), null, 0);?>
				<tr>
					<td class="cell-name">
						<a href="<?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getUserWebPath();?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getProfileAvatarPath(24);?>
" alt="avatar" class="avatar" /></a>
						<p class="username word-wrap"><a href="<?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getUserWebPath();?>
"><?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getLogin();?>
</a>
							<?php if ($_smarty_tpl->tpl_vars['oUserNote']->value){?>
								<i class="icon-comment js-infobox" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['oUserNote']->value->getText(), ENT_QUOTES, 'UTF-8', true);?>
"></i>
							<?php }?>
						</p>
					</td>
					<td class="cell-date"><?php if ($_smarty_tpl->tpl_vars['oSession']->value){?><?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oSession']->value->getDateLast(),'format'=>"d.m.y, H:i"),$_smarty_tpl);?>
<?php }?></td>
					<td class="cell-date"><?php echo smarty_function_date_format(array('date'=>$_smarty_tpl->tpl_vars['oUserList']->value->getDateRegister(),'format'=>"d.m.y, H:i"),$_smarty_tpl);?>
</td>
					<td class="cell-skill"><?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getSkill();?>
</td>
					<td class="cell-rating"><strong><?php echo $_smarty_tpl->tpl_vars['oUserList']->value->getRating();?>
</strong></td>
				</tr>
			<?php } ?>
		<?php }else{ ?>
			<tr>
				<td colspan="5">
					<?php if ($_smarty_tpl->tpl_vars['sUserListEmpty']->value){?>
						<?php echo $_smarty_tpl->tpl_vars['sUserListEmpty']->value;?>

					<?php }else{ ?>
						<?php echo $_smarty_tpl->tpl_vars['aLang']->value['user_empty'];?>

					<?php }?>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>


<?php echo $_smarty_tpl->getSubTemplate ('paging.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('aPaging'=>$_smarty_tpl->tpl_vars['aPaging']->value), 0);?>
<?php }} ?>