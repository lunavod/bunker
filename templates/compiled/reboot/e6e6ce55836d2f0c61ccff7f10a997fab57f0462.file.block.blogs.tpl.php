<?php /* Smarty version Smarty-3.1.8, created on 2015-09-26 11:07:08
         compiled from "/home/w/windro/reboot/public_html/templates/skin/reboot/blocks/block.blogs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15280595225606522c97e974-43084382%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e6e6ce55836d2f0c61ccff7f10a997fab57f0462' => 
    array (
      0 => '/home/w/windro/reboot/public_html/templates/skin/reboot/blocks/block.blogs.tpl',
      1 => 1443254639,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15280595225606522c97e974-43084382',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aLang' => 0,
    'oUserCurrent' => 0,
    'sBlogsTop' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5606522c992e53_21359141',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5606522c992e53_21359141')) {function content_5606522c992e53_21359141($_smarty_tpl) {?><?php if (!is_callable('smarty_function_router')) include '/home/w/windro/reboot/public_html/engine/modules/viewer/plugs/function.router.php';
?><div class="block" id="block_blogs">
	<header class="block-header">
		<h3><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_blogs'];?>
</h3>
		<div class="block-update js-block-blogs-update fa fa-refresh"></div>
	</header>
	
	
	<div class="block-content">
		<ul class="nav nav-pills js-block-blogs-nav">
			<li class="active js-block-blogs-item" data-type="top"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_blogs_top'];?>
</a></li>
			<?php if ($_smarty_tpl->tpl_vars['oUserCurrent']->value){?>
				<li class="js-block-blogs-item" data-type="join"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_blogs_join'];?>
</a></li>
				<li class="js-block-blogs-item" data-type="self"><a href="#"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_blogs_self'];?>
</a></li>
			<?php }?>
		</ul>
		
		
		<div class="js-block-blogs-content">
			<?php echo $_smarty_tpl->tpl_vars['sBlogsTop']->value;?>

		</div>

		
		<footer>
			<a href="<?php echo smarty_function_router(array('page'=>'blogs'),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['aLang']->value['block_blogs_all'];?>
</a>
		</footer>
	</div>
</div>
<?php }} ?>